<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use AppBundle\Entity\Connection;

class ConnectionRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param User $origin
     * @param User $friend
     * @return ConnectionRepository
     */
    public function addConnection(User $origin, User $friend)
    {
        $connection = new Connection();
        $connection->setUserId($origin->getId());
        $connection->setFriendId($friend->getId());
        $connection->setActive(0);
        $connection->setCreatedDt(new \DateTime());

        $this->getEntityManager()->persist($connection);
        $this->getEntityManager()->flush();

        return $this;
    }
}
