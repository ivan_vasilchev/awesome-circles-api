<?php

namespace AC\ApiBundle\Entity;

use AppBundle\Entity\User;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Class UserDTO
 * @package AC\ApiBundle\Entity
 * @ExclusionPolicy("all")
 */
class UserDTO
{
    /**
     * @var string
     * @Expose
     */
    private $handle;

    /**
     * @var string
     * @Expose
     */
    private $first_name;

    /**
     * @var string
     * @Expose
     */
    private $last_name;

    /**
     * @var string
     * @Expose
     */
    private $email;

    /**
     * @var string
     * @Expose
     */
    private $image_url;

    public function __construct(User $user)
    {
        $this->handle = $user->getHandle();
        $this->first_name = $user->getFirstName();
        $this->last_name = $user->getLastName();
        $this->email = $user->getEmail();
        $this->image_url = $user->getImageUrl();
    }
}