<?php

namespace AC\ApiBundle\Entity;

use JMS\Serializer\Annotation\Expose;

/**
 * Class used to represent user action results - such as an invitation
 */
class ActionDTO
{
    /**
     * @var string
     * @Expose
     */
    private $action;

    /**
     * @var string
     * @Expose
     */
    private $status;

    /**
     * @var string
     * @Expose
     */
    private $message;


    public function __construct($action , $status, $message = null )
    {
        $this->action = $action;
        $this->status = $status;
        $this->message = $message;
    }

}