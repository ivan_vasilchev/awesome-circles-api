<?php

namespace AC\ApiBundle\Controller;

use AC\ApiBundle\Entity\ActionDTO;
use AC\ApiBundle\Enum\ApiActionEnum;
use AC\ApiBundle\Enum\ApiActionStatusEnum;
use AC\ApiBundle\Event\UserInviteEvent;
use AC\ApiBundle\Exception\ApiHttpException;
use AC\ApiBundle\Manager\UserManager;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Prefix;
use Symfony\Component\Security\Core\User\User;

/**
 * Class UsersController
 * @package AC\ApiBundle\Controller
 * @Prefix("api/v1")
 */
class UsersController extends FOSRestController
{
    /**
     * @Get("/users/{userid}", name="users_profile", options={"expose"=true})
     * @param Request $request
     */
    public function profileAction(Request $request)
    {
        $profile = $this->getUserManager()->getUserProfile($request->get('userid'));

        $view = $this->view($profile);

        return $this->handleView($view);
    }
    
    /**
     * @Get("/users/{userid}/history", name="users_history", options={"expose"=true})
     * @param Request $request
     */
    public function historyAction(Request $request)
    {
        // TODO: Implement history list
        $view = $this->view(['history' => []]);

        return $this->handleView($view);
    }

    /**
     * @Post("/users/{userid}/invite", name="users_invite", options={"expose"=true})
     * @param Request $request
     */
    public function inviteAction(Request $request)
    {
        $event = new UserInviteEvent($request->get('userid'));
        $event->setOwnerIdentity(new User('vonq',null));
        $event->setRequest($request);

        try {

            $this->container->get('event_dispatcher')->dispatch(UserInviteEvent::EVENT_NAME, $event);

        } catch (ApiHttpException $ae) {
            return $this->sendException($ae);
        } catch (\Exception $e) {
            return $this->sendError('An error occured while trying to invite a friend :' . $e->getMessage(), 0);
        }

        $actionResult = new ActionDTO(ApiActionEnum::INVITATION, ApiActionStatusEnum::OK);

        return $this->handleView($this->view($actionResult));
    }

    /**
     * @Get("/users/{userid}/friends", name="users_friends", options={"expose"=true})
     * @param Request $request
     * @return View
     */
    public function friendsAction(Request $request)
    {
        try {
            $view = $this->view($this->getUserManager()->getUserFriends(
                $request->get('userid')
            ));
        } catch (ApiHttpException $ae) {
            return $this->sendException($ae);
        } catch (\Exception $e) {
            return $this->sendError('An error occured while trying to serve your request', 0);
        }

        return $this->handleView($view);
    }

    /**
     * @Get("/users", name="users_list", options={"expose"=true})
     * @param Request $request
     */
    public function listAction(Request $request)
    {
        try {

            $view = $this->view($this->getUserManager()->getUsers());

        } catch (ApiHttpException $ae) {
            return $this->sendException($ae);
        } catch (\Exception $e) {
            return $this->sendError('An error occured while trying to serve your request', 0);
        }

        return $this->handleView($view);
    }

    /**
     * @Get("/online", options={"expose"=true}, name="users_online")
     * @param Request $request
     */
    public function onlineAction(Request $request)
    {
        try {

            $view = $this->view($this->getUserManager()->getUsers());

        } catch (ApiHttpException $ae) {
            return $this->sendException($ae);
        } catch (\Exception $e) {
            return $this->sendError('An error occured while trying to serve your request', 0);
        }

        return $this->handleView($view);
    }

    /**
     * @return UserManager
     */
    private function getUserManager()
    {
        return $this->get('ac_api.user_manager');
    }

    /**
     * @param $message
     * @param $code
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function sendError($message, $code)
    {
        $view = $this->view([
            'message' => $message,
            'code' => $code
        ]);

        return $this->handleView($view);
    }

    /**
     * @param ApiHttpException $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function sendException(ApiHttpException $exception)
    {
        return $this->sendError($exception->getMessage(), $exception->getCode());
    }
}