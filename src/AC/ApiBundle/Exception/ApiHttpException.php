<?php

namespace AC\ApiBundle\Exception;

use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * Class ApiHttpException - used to capture events occuring in the API and also prevent leaks of internal exceptions going to the user
 * @package AC\ApiBundle\Exception
 * @ExclusionPolicy("all")
 */
class ApiHttpException extends \Exception
{
    /**
     * @var int
     * @Expose
     */
    protected $code = 1;

    /**
     * @var string
     * @Expose
     */
    protected $message = 'An API Call Exception has occured.';
}