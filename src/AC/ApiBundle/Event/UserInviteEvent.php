<?php

namespace AC\ApiBundle\Event;

class UserInviteEvent extends ApiEvent
{
    const EVENT_NAME = 'api.user.invite';

    private $recipientHandle;

    private $status;

    public function __construct($recipientHandle)
    {
        $this->recipientHandle = $recipientHandle;
    }

    /**
     * @return mixed
     */
    public function getRecipientHandle()
    {
        return $this->recipientHandle;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

}