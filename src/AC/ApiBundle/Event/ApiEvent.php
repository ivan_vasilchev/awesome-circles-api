<?php

namespace AC\ApiBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiEvent - Encapsulates and API Event
 * Should be used as a mechanism to capture all events in the system in a predictable manner and allow for further storage and analysis
 * @package AC\ApiBundle\Event
 */
abstract class ApiEvent extends Event
{
    const EVENT_NAME = null;

    /**
     * @var UserInterface
     */
    private $ownerIdentity = null;

    /**
     * @var Request
     */
    private $request;

    /**
     * Returns "Owner" of the event - The user that initiated the action
     * @return UserInterface
     */
    public function getOwnerIdentity()
    {
        return $this->ownerIdentity;
    }

    /**
     * @param UserInterface $user
     */
    public function setOwnerIdentity(UserInterface $user)
    {
        $this->ownerIdentity = $user;
    }

    /**
     * Set original request data - useful for capturing client metadata
     * @param Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return null
     */
    public static function getEventName()
    {
        return static::EVENT_NAME;
    }

}