<?php

namespace AC\ApiBundle\EventSubscriber;

use AC\ApiBundle\Event\UserInviteEvent;
use AC\ApiBundle\Manager\UserManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;

class UserInvitationEventSubscriber implements EventSubscriberInterface {

    use ContainerAwareTrait;

    var $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            UserInviteEvent::EVENT_NAME => [
                ['processInvitation', 10],
                ['logInvitation', 5],
                ['emailInvitation', 0],
                ['sendToAnalyticsService', 0]
            ]
        ];
    }

    /**
     * @param UserInviteEvent $event
     */
    public function processInvitation(UserInviteEvent $event)
    {
        $this->userManager->inviteUser($event->getOwnerIdentity(), $event->getRecipientHandle());
    }

    /**
     * @param UserInviteEvent $event
     */
    public function logInvitation(UserInviteEvent $event)
    {
        $who = null;

        if ($event->getOwnerIdentity() instanceof User) {
            $who = $event->getOwnerIdentity()->getUsername();
        } elseif (is_string($event->getOwnerIdentity())) {
            $who = $event->getOwnerIdentity();
        } else {
            $who = 'UNIDENTIFIED';
        }

        $this->container->get('logger')->info('Invitation was sent to user '. $event->getRecipientHandle(). ' by ' . $who);
    }

    /**
     * @param UserInviteEvent $event
     */
    public function emailInvitation(UserInviteEvent $event)
    {
        // TODO: Send job to a hypothetical email worker
    }

    /**
     * @param UserInviteEvent $event
     */
    public function sendToAnalyticsService(UserInviteEvent $event)
    {
        // TODO: Send to data-mining service
    }
}