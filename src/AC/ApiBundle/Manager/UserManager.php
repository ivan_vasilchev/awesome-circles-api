<?php

namespace AC\ApiBundle\Manager;

use AC\ApiBundle\Entity\UserDTO;
use AC\ApiBundle\Enum\ApiErrorEnum;
use AC\ApiBundle\Exception\ApiHttpException;
use AppBundle\Repository\UserRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;

/**
 * Class UserManager
 * @package AC\ApiBundle\Manager
 */
class UserManager
{
    const DEFAULT_LIMIT = 10;

    /**
     * @var EntityManager
     */
    private $orm;


    public function __construct($orm)
    {
        $this->orm = $orm;
    }

    /**
     * @param array $filter
     * @return array
     */
    public function getUsers($filter = [])
    {
        $results = [];
        foreach ($this->getRepository()->findAll() as $user) {
            $results [] = new UserDTO($user);
        }
        return $results;
    }

    /**
     * @param $userHandle
     * @param array $filter
     * @return array
     */
    public function getUserFriends($userHandle, $filter = [])
    {
        $entity = $this->getRepository()->findOneBy(['handle' => $userHandle]);

        if ($entity == null) {
            throw new ApiHttpException('User was not found', ApiErrorEnum::USER_NOT_FOUND);
        }

        $friends = $this->getRepository()->findUserFriends($entity);

        $results = [];
        foreach ($friends as $link) {
            $results[] = new UserDTO($link);
        }

        return $results;
    }


    /**
     * @param $from
     * @param $userHandle
     * @throws ApiHttpException
     */
    public function inviteUser($from, $userHandle)
    {
        $entity = $this->getRepository()->findOneBy(['handle' => $userHandle]);

        if ($entity == null) {
            throw new ApiHttpException('User was not found', ApiErrorEnum::USER_NOT_FOUND);
        }

        $connectionRepo = $this->orm->getRepository(\AppBundle\Entity\Connection::class);

        $connectionRepo->addConnection($this->getOwnerUser(), $entity);
    }


    /**
     * @param $userHandle
     * @return UserDTO
     */
    public function getUserProfile($userHandle)
    {
        $user = $this->getRepository()->findOneBy(['handle' => $userHandle]);

        if ($user == null) {
            throw new ApiHttpException('User was not found', ApiErrorEnum::USER_NOT_FOUND);
        }

        return new UserDTO($user);
    }

    /**
     * @return User
     * @throws ApiHttpException
     */
    private function getOwnerUser()
    {
        $myself = $this->getRepository()->find(1);

        if ($myself == null) {
            throw new ApiHttpException('Unable to find a valid owner user. No migrations ran?', ApiErrorEnum::MYSTICAL_ERROR);
        }

        return $myself;
    }


    /**
     * @return UserRepository
     */
    private function getRepository()
    {
        return $this->orm->getRepository(\AppBundle\Entity\User::class);
    }
}