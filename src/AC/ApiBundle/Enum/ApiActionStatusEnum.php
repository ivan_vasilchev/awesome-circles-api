<?php

namespace AC\ApiBundle\Enum;

/**
 * Class ApiActionStatusEnum - Api action statuses used for ActionDTOs
 * @package AC\ApiBundle\Enum
 */
final class ApiActionStatusEnum
{
    const OK = 'OK';
    const PENDING = 'PENDING';
    const CANCELED = 'CANCELED';
}