<?php

namespace AC\ApiBundle\Enum;


final class ApiErrorEnum
{
    const GENERIC_ERROR = 101001; // Any generic exception that occurs and is not handled by default
    const USER_NOT_FOUND = 101002; // User not found
    const MYSTICAL_ERROR = 101003; // An error that does not have any explanation. Probably a sanity check failed.
}