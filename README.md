PoC API For A Social Network   
====

Installation 
====

The installation of the project is as simple as running : 

` composer install # install dependencies and configure database `

` bin/console doctrine:database:create # Create database `

` bin/console doctrine:schema:create # Create schema ` 

` bin/console faker:populate # Populates some Fake user data ` 

` bin/console doctrine:migrations:migrate # ... and connection data ` 


A virtual host could also be configured or the Symfony internal WEB server can be used 


Design Considerations
====

Backend 
==== 

The backend is generally separated in two bundles 

- ApiBundle - Containing API logic for REST services 

The ApiBundle contains the logic for the REST endpoints, which is handled by the UserController actions. 
The controller extends FOSRestController class coming from the FOSRestBunlde which handles REST serialization. 
The endpoints return JSON serialized DTOs, separating them from the real underlying entities. 

The controller depends on the UserManager service to provide necessary functionality which in turn uses the UserRepository 
for data retrieval and persistence. 

Some actions (currently only user invitation) can dispatch events which are handled by dedicated Event Subscribers. 
This provides a mechanism for adding decoupled way of further seamless extension.  

    
- AppBundle - Containing basic presentation logic (HTML, JS, etc.) 

This is just a DefaultController serving the only Twig view HTML


Database 
====
For a database the Doctrine ORM is used with some native query calls (such as friends lists) for fine grain tuning.  


API 
====

The API is a REST API employing JSON serialization, chosen as a lightweight and widely adopted scheme.  

Endpoints are pefixed with a version number to allow for backwards compatibility upon new (possibly breaking) changes introduced in the system. 

GET /users/{userid} - Gets user profile information 

POST /users/{userid}/invite - Send an invitation

GET /users/{userid}/friends - Get user's friend list  

GET /users - Retrieves a list of users (filtering and paging is yet to come)

GET /online - Retrieves a list of "online" users 


Frontend
====

The frontend is using jQuery for simple DOM manipulation and UI. Also Handlebars templating is introduced for easier dynamic HTML management. 

For API access there is a JS script provided that encapsulates the API operations. 


Known shortcomings 
====

- No user authentication, although a token scheme is included in the API requests. All invites will be assigned to the first user in the database (user id 1). 
This could be done utilizing Symfony Firewalls for management. 

- No test coverage, whatsoever. 

- No filtering and paging for users list. This is intended to utilize the HateoasBundle's PaginatedCollection class and serialization. 

- Probably some documentation on methods and classes is missing 