
var api = function(token) {

    this.token = token;

    // Returns a list of online users
    api.prototype.getOnlineUsers = function (handler, out) {
        this.handleGetRequest('onlineusers_online', null, handler, out);
    }

    // Returns the list of friends for a particular user
    api.prototype.getUserFriends = function (userHandle, handler, out) {
        this.handleGetRequest('friendsusers_friends', {userid: userHandle}, handler, out);
    }

    // Sends an invitation to user
    api.prototype.inviteUser = function (userHandle, handler) {
        this.handlePostRequest('inviteusers_invite', { userid : userHandle}, {} , handler);
    }

    // Returns a list of users
    api.prototype.getUsers = function (params, handler) {
        this.handleGetRequest('listusers_list', params, handler);
    }

    // Returns a list of users
    api.prototype.getUserProfile = function (userHandle, handler) {
        this.handleGetRequest('profileusers_profile', { userid : userHandle}, handler);
    }

    api.prototype.getToken = function() {
        return this.token;
    }

    // Generic GET request handling
    api.prototype.handleGetRequest = function (route, routeParams, handler, out) {
        $.get(Routing.generate(route, routeParams) + '?token=' + this.getToken(), function(data){
            handler(data, out);
        });
    }

    // Generic POST request handling
    api.prototype.handlePostRequest = function (route, routeParams, data, handler) {
        var url = Routing.generate(route, routeParams);
        $.post(url, data, function(data){
            handler(data);
        });
    }

};