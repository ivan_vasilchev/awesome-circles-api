<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use AppBundle\Entity\Connection;
use AppBundle\Entity\User;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161102223443 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $users;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $userRepo = $this->getRepository(User::class);

        $this->users = $userRepo->findAll();

        foreach ($this->users as $user) {
            foreach (array_rand($this->users, rand(5, 15)) as $friend) {
                $connection = new Connection();
                $connection->setUserId($user->getId());
                $connection->setActive(1);
                $connection->setCreatedDt(new \DateTime());
                $connection->setFriendId($this->users[$friend]->getId());
                $this->getDoctrine()->getEntityManager()->persist($connection);
            }
        }

        $this->getDoctrine()->getEntityManager()->flush();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    /**
     * @return EntityRepository
     */
    private function getRepository($class)
    {
        return $this->getDoctrine()->getRepository($class);
    }

    private function getDoctrine()
    {
        return $this->container->get('doctrine');
    }

}
